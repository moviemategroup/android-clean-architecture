package io.fajarca.project.moviemate.data.source
import io.fajarca.project.moviemate.data.service.MovieService
import io.fajarca.project.moviemate.extension.runBlockingTest
import io.fajarca.project.moviemate.rule.CoroutineTestRule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class MovieRemoteDataSourceTest {

    private lateinit var sut: MovieRemoteDataSource

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @Mock
    lateinit var movieService: MovieService

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        sut = MovieRemoteDataSource(movieService)
    }

    @Test
    fun shouldGetNowPlayingFromMovieServiceWhenInvoked() = coroutineTestRule.runBlockingTest {
        sut.getNowPlayingMovies(coroutineTestRule.testDispatcherProvider.io)
        verify(movieService).nowPlaying()
    }

    @Test
    fun shouldGetMovieDetailFromMovieServiceWhenInvoked() = coroutineTestRule.runBlockingTest {
        val movieId = 3349
        sut.getMovieDetail(coroutineTestRule.testDispatcherProvider.io, movieId)
        verify(movieService).detail(movieId)
    }
}
