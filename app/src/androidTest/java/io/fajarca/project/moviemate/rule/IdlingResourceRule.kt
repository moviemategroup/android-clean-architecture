package io.fajarca.project.moviemate.rule

import androidx.test.espresso.IdlingRegistry
import io.fajarca.project.moviemate.helper.IdlingResourceWrapper
import org.junit.rules.TestWatcher
import org.junit.runner.Description

class IdlingResourceRule : TestWatcher() {
    override fun starting(description: Description?) {
        IdlingRegistry.getInstance().register(IdlingResourceWrapper.idlingResource)
        super.starting(description)
    }

    override fun finished(description: Description?) {
        IdlingRegistry.getInstance().unregister(IdlingResourceWrapper.idlingResource)
        super.finished(description)
    }
}
