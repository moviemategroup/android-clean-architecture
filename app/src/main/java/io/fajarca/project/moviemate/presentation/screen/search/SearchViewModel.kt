package io.fajarca.project.moviemate.presentation.screen.search

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import io.fajarca.project.moviemate.domain.entity.search.SearchResultUiModel
import io.fajarca.project.moviemate.domain.usecase.search.SearchUseCase
import kotlinx.coroutines.flow.Flow

class SearchViewModel @ViewModelInject constructor(
    private val searchUseCase: SearchUseCase
) : ViewModel() {

    fun search(query: String, includeAdult: Boolean): Flow<PagingData<SearchResultUiModel>> {
        return searchUseCase(query, includeAdult).cachedIn(viewModelScope)
    }
}
