package io.fajarca.project.moviemate.presentation.screen.search

import io.fajarca.project.extensions.loadImage
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseViewHolder
import io.fajarca.project.moviemate.databinding.ItemSearchResultBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.search.MediaType
import io.fajarca.project.moviemate.domain.entity.search.SearchResultUiModel

class SearchResultViewHolder(private val binding: ItemSearchResultBinding) :
    BaseViewHolder<SearchResultUiModel>(binding) {

    companion object {
        const val LAYOUT = R.layout.item_search_result
    }

    override fun bind(item: SearchResultUiModel, clickListener: ItemClickListener) {
        when (item.mediaType) {
            MediaType.MOVIE -> {
                binding.tvMediaType.text = "Movie"
                binding.tvTitle.text = item.name
            }
            MediaType.SERIES -> {
                binding.tvTitle.text = item.name
                binding.tvMediaType.text = "Series"
            }
            else -> {
                binding.tvMediaType.text = "Person"
                binding.tvTitle.text = item.name
            }
        }
        binding.tvRating.text = item.voteAverage.toString()
        binding.ivPoster.loadImage(item.imageUrl)
        binding.executePendingBindings()
        binding.root.setOnClickListener { clickListener.onClick(item) }
    }
}
