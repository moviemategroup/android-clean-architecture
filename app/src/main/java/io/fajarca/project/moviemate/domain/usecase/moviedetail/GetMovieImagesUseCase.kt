package io.fajarca.project.moviemate.domain.usecase.moviedetail

import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieImage
import io.fajarca.project.moviemate.domain.repository.MovieRepository
import javax.inject.Inject

class GetMovieImagesUseCase @Inject constructor(private val repository: MovieRepository) {

    suspend operator fun invoke(movieId: Int): List<MovieImage> {
        val apiResult = repository.getMovieImages(movieId)
        return when (apiResult) {
            is Result.Success -> apiResult.data
            is Result.Error -> emptyList()
            else -> emptyList()
        }
    }
}
