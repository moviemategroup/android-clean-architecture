package io.fajarca.project.moviemate.domain.entity.moviedetail

import io.fajarca.project.moviemate.abstraction.BaseItemModel
import io.fajarca.project.moviemate.presentation.factory.ItemTypeFactory

data class SimilarMovie(
    val id: Int,
    val posterPath: String,
    val title: String,
    val voteAverage: Float,
    val adult: Boolean
) : BaseItemModel() {
    override fun type(itemTypeFactory: ItemTypeFactory): Int {
        return itemTypeFactory.type(this)
    }
}
