package io.fajarca.project.moviemate.presentation.screen.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import io.fajarca.project.moviemate.databinding.ItemLoadStateBinding

class SearchLoadStateAdapter : LoadStateAdapter<SearchLoadStateAdapter.LoadStateViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder {
        return LoadStateViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    class LoadStateViewHolder(private val binding: ItemLoadStateBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(loadState: LoadState) {
            binding.progressBar.isVisible = loadState is LoadState.Loading
            binding.executePendingBindings()
        }

        companion object {
            fun create(parent: ViewGroup): LoadStateViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemLoadStateBinding.inflate(layoutInflater, parent, false)
                return LoadStateViewHolder(binding)
            }
        }
    }


}
