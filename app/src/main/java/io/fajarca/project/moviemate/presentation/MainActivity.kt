package io.fajarca.project.moviemate.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import dagger.hilt.android.AndroidEntryPoint
import io.fajarca.project.extensions.gone
import io.fajarca.project.extensions.makeStatusBarTransparent
import io.fajarca.project.extensions.visible
import io.fajarca.project.moviemate.R
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val navController by lazy { Navigation.findNavController(this, R.id.navHostFragment) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupNavigation()
        makeStatusBarTransparent()
    }

    private fun setupNavigation() {
        bottomNavigationView.setupWithNavController(navController)
    }

    fun showBottomNavigation() {
        bottomNavigationView.visible()
    }

    fun hideBottomNavigation() {
        bottomNavigationView.gone()
    }
}
