package io.fajarca.project.moviemate.data.response.movies

import com.squareup.moshi.Json

data class MovieVideosDto(
    @Json(name = "results")
    val results: List<Result?>? = null
) {
    data class Result(
        @Json(name = "id")
        val id: String? = null,
        @Json(name = "key")
        val key: String? = null,
        @Json(name = "name")
        val name: String? = null,
        @Json(name = "site")
        val site: String? = null,
        @Json(name = "size")
        val size: Int? = null,
        @Json(name = "type")
        val type: String? = null
    )
}
