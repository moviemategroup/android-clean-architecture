package io.fajarca.project.moviemate.domain.usecase.moviedetail

import io.fajarca.project.extensions.toLocalDate
import io.fajarca.project.moviemate.abstraction.UseCase
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieDetail
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieDetailUiModel
import io.fajarca.project.moviemate.domain.repository.MovieRepository
import javax.inject.Inject

class GetMovieDetailUseCase @Inject constructor(private val repository: MovieRepository) :
    UseCase<MovieDetailUiModel?, Int>() {

    companion object {
        const val DIVIDER = 60
    }

    override suspend fun invoke(params: Int): MovieDetailUiModel? {
        val apiResult = repository.getMovieDetail(params)
        return when (apiResult) {
            is Result.Success -> mapData(apiResult.data)
            else -> null
        }
    }

    private fun mapData(input: MovieDetail): MovieDetailUiModel {
        return MovieDetailUiModel(
            input.id,
            input.title,
            input.originalTitle,
            input.overview,
            input.posterPath,
            input.backdropPath,
            input.voteCount,
            input.voteAverage,
            input.popularity,
            transformReleaseData(input.releaseDate),
            input.adult,
            transformRuntime(input.runtime),
            input.tagline,
            input.genres
        )
    }

    private fun transformReleaseData(releaseDate: String): String {
        return releaseDate.toLocalDate("yyyy-MM-dd", "MMMM yyyy")
    }

    private fun transformRuntime(runtime: Int): String {
        val hours = runtime / DIVIDER
        val minutes = runtime % DIVIDER
        return if (hours > 0) {
            "${hours}h ${minutes}m"
        } else {
            "${minutes}m"
        }
    }
}
