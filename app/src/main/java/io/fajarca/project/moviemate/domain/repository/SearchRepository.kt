package io.fajarca.project.moviemate.domain.repository

import androidx.paging.PagingData
import io.fajarca.project.moviemate.domain.entity.search.SearchResult
import kotlinx.coroutines.flow.Flow

interface SearchRepository {
    fun multiSearch(query: String, includeAdult: Boolean): Flow<PagingData<SearchResult>>
}
