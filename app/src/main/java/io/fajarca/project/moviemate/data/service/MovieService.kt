package io.fajarca.project.moviemate.data.service

import io.fajarca.project.moviemate.data.response.movies.CastDto
import io.fajarca.project.moviemate.data.response.movies.MovieDetailDto
import io.fajarca.project.moviemate.data.response.movies.MovieImagesDto
import io.fajarca.project.moviemate.data.response.movies.MovieVideosDto
import io.fajarca.project.moviemate.data.response.movies.NowPlayingMovieDto
import io.fajarca.project.moviemate.data.response.movies.PopularMovieDto
import io.fajarca.project.moviemate.data.response.movies.SimilarMoviesDto
import io.fajarca.project.moviemate.data.response.movies.TopRatedMovieDto
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieService {

    @GET("movie/now_playing")
    suspend fun nowPlaying(): NowPlayingMovieDto

    @GET("movie/{movieId}")
    suspend fun detail(@Path("movieId") movieId: Int): MovieDetailDto

    @GET("movie/popular")
    suspend fun popular(): PopularMovieDto

    @GET("movie/top_rated")
    suspend fun topRated(): TopRatedMovieDto

    @GET("movie/{movieId}/credits")
    suspend fun credits(@Path("movieId") movieId: Int): CastDto

    @GET("movie/{movieId}/images")
    suspend fun images(@Path("movieId") movieId: Int): MovieImagesDto

    @GET("movie/{movieId}/similar")
    suspend fun similar(@Path("movieId") movieId: Int): SimilarMoviesDto

    @GET("movie/{movieId}/videos")
    suspend fun videos(@Path("movieId") movieId: Int): MovieVideosDto
}
