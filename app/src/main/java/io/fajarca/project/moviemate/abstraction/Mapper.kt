package io.fajarca.project.moviemate.abstraction

abstract class Mapper<in I, out O> {
    abstract fun map(input: I): O
}
