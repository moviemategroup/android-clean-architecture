package io.fajarca.project.moviemate.data.response.movies

import com.squareup.moshi.Json

data class MovieImagesDto(
    @field:Json(name = "backdrops")
    val posters: List<Poster?> = emptyList()
) {
    data class Poster(
        @field:Json(name = "aspect_ratio")
        val aspectRatio: Float? = null,
        @field:Json(name = "file_path")
        val filePath: String? = null,
        @field:Json(name = "height")
        val height: Int? = null,
        @field:Json(name = "width")
        val width: Int? = null
    )
}
