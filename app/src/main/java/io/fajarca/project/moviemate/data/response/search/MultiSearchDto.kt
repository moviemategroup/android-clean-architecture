package io.fajarca.project.moviemate.data.response.search

import com.squareup.moshi.Json

data class MultiSearchDto(
    @field:Json(name = "results")
    val results: List<Result> = emptyList()
) {
    data class Result(
        @field:Json(name = "adult")
        val adult: Boolean? = null,
        @field:Json(name = "first_air_date")
        val firstAirDate: String? = null,
        @field:Json(name = "id")
        val id: Int? = null,
        @field:Json(name = "media_type")
        val mediaType: String? = null,
        @field:Json(name = "name")
        val name: String? = null,
        @field:Json(name = "title")
        val title: String? = null,
        @field:Json(name = "poster_path")
        val posterPath: String? = null,
        @field:Json(name = "profile_path")
        val profilePath: String? = null,
        @field:Json(name = "release_date")
        val releaseDate: String? = null,
        @field:Json(name = "vote_average")
        val voteAverage: Float? = null
    )
}
