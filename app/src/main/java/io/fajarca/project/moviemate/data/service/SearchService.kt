package io.fajarca.project.moviemate.data.service

import io.fajarca.project.moviemate.data.response.search.MultiSearchDto
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchService {
    @GET("search/multi")
    suspend fun multiSearch(
        @Query("query") query: String,
        @Query("page") page: Int,
        @Query("include_adult") includeAdult: Boolean
    ): MultiSearchDto
}
