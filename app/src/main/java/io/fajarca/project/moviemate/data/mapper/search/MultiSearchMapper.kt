package io.fajarca.project.moviemate.data.mapper.search

import io.fajarca.project.moviemate.abstraction.Mapper
import io.fajarca.project.moviemate.data.response.search.MultiSearchDto
import io.fajarca.project.moviemate.domain.entity.search.SearchResult
import javax.inject.Inject

class MultiSearchMapper @Inject constructor() : Mapper<MultiSearchDto.Result, SearchResult>() {

    override fun map(input: MultiSearchDto.Result): SearchResult {
        return SearchResult(
            input.adult ?: false,
            input.firstAirDate ?: "",
            input.id ?: 0,
            input.mediaType ?: "",
            input.name ?: "",
            input.title ?: "",
            input.posterPath ?: "0f",
            input.releaseDate ?: "",
            input.voteAverage ?: 0.0f,
            input.profilePath ?: ""
        )

    }
}
