package io.fajarca.project.moviemate.helper.bindingadapter

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import io.fajarca.project.config.constant.MovieConstant
import io.fajarca.project.config.constant.MovieConstant.GLIDE_THUMBNAIL_SIZE_MULTIPLIER
import io.fajarca.project.extensions.R

@BindingAdapter("loadImage")
fun loadPortraitImage(view: ImageView, imageUrl: String?) {
    if (imageUrl.isNullOrEmpty()) return

    val url = MovieConstant.IMAGE_BASE_URL_POSTER + imageUrl

    Glide.with(view.context)
        .load(url)
        .error(ContextCompat.getDrawable(view.context, R.drawable.ic_broken_image))
        .placeholder(ColorDrawable(Color.LTGRAY))
        .transition(DrawableTransitionOptions.withCrossFade())
        .thumbnail(GLIDE_THUMBNAIL_SIZE_MULTIPLIER)
        .apply(RequestOptions.fitCenterTransform())
        .into(view)
}

@BindingAdapter("loadBackdrop")
fun loadBackdrop(view: ImageView, imageUrl: String?) {
    if (imageUrl.isNullOrEmpty()) return

    val url = MovieConstant.IMAGE_BASE_URL_BACKDROP + imageUrl

    Glide.with(view.context)
        .load(url)
        .error(ContextCompat.getDrawable(view.context, R.drawable.ic_broken_image))
        .placeholder(ColorDrawable(Color.LTGRAY))
        .transition(DrawableTransitionOptions.withCrossFade())
        .thumbnail(GLIDE_THUMBNAIL_SIZE_MULTIPLIER)
        .apply(RequestOptions.fitCenterTransform())
        .into(view)
}
