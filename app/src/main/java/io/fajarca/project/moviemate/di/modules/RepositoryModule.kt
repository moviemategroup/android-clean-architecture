package io.fajarca.project.moviemate.di.modules

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import io.fajarca.project.moviemate.data.repository.MovieRepositoryImpl
import io.fajarca.project.moviemate.data.repository.SearchRepositoryImpl
import io.fajarca.project.moviemate.data.repository.SeriesRepositoryImpl
import io.fajarca.project.moviemate.domain.repository.MovieRepository
import io.fajarca.project.moviemate.domain.repository.SearchRepository
import io.fajarca.project.moviemate.domain.repository.SeriesRepository

@InstallIn(ApplicationComponent::class)
@Module
interface RepositoryModule {
    @Binds
    fun bindRepository(repository: MovieRepositoryImpl): MovieRepository

    @Binds
    fun bindSeriesRepository(repository: SeriesRepositoryImpl): SeriesRepository

    @Binds
    fun bindSearchRepository(repository: SearchRepositoryImpl): SearchRepository
}
