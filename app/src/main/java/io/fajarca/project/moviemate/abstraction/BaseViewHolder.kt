package io.fajarca.project.moviemate.abstraction

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import io.fajarca.project.moviemate.domain.entity.ItemClickListener

abstract class BaseViewHolder<T>(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    abstract fun bind(item: T, clickListener: ItemClickListener)
}
