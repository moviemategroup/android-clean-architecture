package io.fajarca.project.moviemate.presentation.screen.moviedetail.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseViewHolder
import io.fajarca.project.moviemate.databinding.ItemCastBinding
import io.fajarca.project.moviemate.databinding.ItemCastContainerBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.moviedetail.Cast
import io.fajarca.project.moviemate.domain.entity.moviedetail.CastSection

class CastViewHolder(private val binding: ItemCastContainerBinding) :
    BaseViewHolder<CastSection>(binding) {

    private lateinit var adapter: CastAdapter

    companion object {
        const val LAYOUT = R.layout.item_cast
    }

    override fun bind(item: CastSection, clickListener: ItemClickListener) {
        binding.tvHeader.text = item.header
        adapter = CastAdapter(item.casts, clickListener)
        binding.recyclerView.layoutManager =
            LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.setHasFixedSize(true)
        binding.recyclerView.isNestedScrollingEnabled = false
        binding.executePendingBindings()
    }

    class CastAdapter(private val items: List<Cast>, private val clickListener: ItemClickListener) :
        RecyclerView.Adapter<CastAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder.create(parent)
        }

        override fun getItemCount(): Int = items.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(items[position], clickListener)
        }

        class ViewHolder(private val binding: ItemCastBinding) :
            RecyclerView.ViewHolder(binding.root) {

            fun bind(cast: Cast, clickListener: ItemClickListener) {
                binding.cast = cast
                binding.root.setOnClickListener { clickListener.onClick(cast) }
                binding.executePendingBindings()
            }

            companion object {
                fun create(viewGroup: ViewGroup): ViewHolder {
                    val layoutInflater = LayoutInflater.from(viewGroup.context)
                    val binding = ItemCastBinding.inflate(layoutInflater, viewGroup, false)
                    return ViewHolder(binding)
                }
            }
        }
    }
}
