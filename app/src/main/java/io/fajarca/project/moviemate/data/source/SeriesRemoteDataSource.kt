package io.fajarca.project.moviemate.data.source

import io.fajarca.project.moviemate.data.response.series.OnTheAirDto
import io.fajarca.project.moviemate.data.response.series.PopularSeriesDto
import io.fajarca.project.moviemate.data.response.series.TopRatedSeriesDto
import io.fajarca.project.moviemate.data.service.SeriesService
import io.fajarca.project.moviemate.data.vo.Result
import javax.inject.Inject
import kotlinx.coroutines.CoroutineDispatcher

class SeriesRemoteDataSource @Inject constructor(private val seriesService: SeriesService) : RemoteDataSource() {

    suspend fun getPopularSeries(dispatcher: CoroutineDispatcher): Result<PopularSeriesDto> {
        return safeApiCall(dispatcher) { seriesService.popular() }
    }

    suspend fun getTopRatedSeries(dispatcher: CoroutineDispatcher): Result<TopRatedSeriesDto> {
        return safeApiCall(dispatcher) { seriesService.topRated() }
    }

    suspend fun onTheAirSeries(dispatcher: CoroutineDispatcher): Result<OnTheAirDto> {
        return safeApiCall(dispatcher) { seriesService.onTheAir() }
    }
}
