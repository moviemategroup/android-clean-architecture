package io.fajarca.project.moviemate.di.modules

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import io.fajarca.project.config.constant.SharedPreferenceConstant

@InstallIn(ApplicationComponent::class)
@Module
class SharedPreferenceModule {

    @Provides
    fun providesPreference(context: Context): SharedPreferences = context.getSharedPreferences(
        SharedPreferenceConstant.PREF_NAME, Context.MODE_PRIVATE
    )

    @Provides
    fun providesSharedPreference(sharedPreferences: SharedPreferences): SharedPreferences.Editor =
        sharedPreferences.edit()
}
