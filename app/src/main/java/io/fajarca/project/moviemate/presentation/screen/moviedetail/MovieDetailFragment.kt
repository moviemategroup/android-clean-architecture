package io.fajarca.project.moviemate.presentation.screen.moviedetail

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import io.fajarca.project.extensions.gone
import io.fajarca.project.extensions.toast
import io.fajarca.project.extensions.visible
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseFragment
import io.fajarca.project.moviemate.abstraction.BaseItemModel
import io.fajarca.project.moviemate.abstraction.BaseRecyclerViewAdapter
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.databinding.FragmentMovieDetailBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.moviedetail.CastSection
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieImageSection
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieVideo
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieVideosSection
import io.fajarca.project.moviemate.domain.entity.moviedetail.SimilarMovieSection
import io.fajarca.project.moviemate.helper.IdlingResourceWrapper
import io.fajarca.project.moviemate.presentation.MainActivity
import io.fajarca.project.moviemate.presentation.factory.ItemTypeFactoryImpl

@AndroidEntryPoint
class MovieDetailFragment : BaseFragment<FragmentMovieDetailBinding>(),
    ItemClickListener {

    private val args by navArgs<MovieDetailFragmentArgs>()
    override fun getLayoutResourceId() = R.layout.fragment_movie_detail
    private val viewModel: MovieDetailViewModel by viewModels()

    private val adapter by lazy {
        BaseRecyclerViewAdapter(
            ItemTypeFactoryImpl(),
            arrayListOf(),
            this
        )
    }
    private val genreAdapter by lazy {
        BaseRecyclerViewAdapter(
            ItemTypeFactoryImpl(),
            arrayListOf(),
            this
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initGenreRecyclerView()
        viewModel.getMovieDetail(args.movieId)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeMovieDetailResult()
    }

    private fun observeMovieDetailResult() {
        viewModel.movie.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> {
                    binding.progressBar.visible()
                    binding.contentGroup.gone()
                    IdlingResourceWrapper.increment()
                }
                is Result.Success -> {
                    binding.movie = it.data.detail
                    genreAdapter.refreshItems(it.data.detail.genres)
                    if (it.data.casts.isNotEmpty()) adapter.addItems(
                        listOf(
                            CastSection(
                                "Top Billed Cast (${it.data.casts.size})",
                                it.data.casts
                            )
                        )
                    )
                    if (it.data.images.isNotEmpty()) adapter.addItems(
                        listOf(
                            MovieImageSection(
                                "Images (${it.data.images.size})",
                                it.data.images
                            )
                        )
                    )
                    if (it.data.videos.isNotEmpty()) adapter.addItems(
                        listOf(
                            MovieVideosSection(
                                "Videos (${it.data.videos.size})",
                                it.data.videos
                            )
                        )
                    )
                    if (it.data.similar.isNotEmpty()) adapter.addItems(
                        listOf(
                            SimilarMovieSection(
                                "People also watch (${it.data.similar.size})",
                                it.data.similar
                            )
                        )
                    )
                    binding.progressBar.gone()
                    binding.contentGroup.visible()
                    IdlingResourceWrapper.decrement()
                }
                is Result.Error -> {
                    binding.progressBar.gone()
                    binding.contentGroup.gone()
                    IdlingResourceWrapper.decrement()
                }
            }
        })
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(requireActivity())
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = adapter
    }

    private fun initGenreRecyclerView() {
        val layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        binding.genreRecyclerView.layoutManager = layoutManager
        binding.genreRecyclerView.adapter = genreAdapter
    }

    override fun onClick(item: BaseItemModel) {
        if (item is MovieVideo) {
            playInYoutube(item.key)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as MainActivity).hideBottomNavigation()
    }

    override fun onDetach() {
        (activity as MainActivity).showBottomNavigation()
        super.onDetach()
    }

    private fun playInYoutube(videoId: String) {
        try {
            val intent =
                Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://$videoId"))
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            toast("Sorry. We're currently unable to play this video")
            e.printStackTrace()
        }
    }
}
