package io.fajarca.project.moviemate.data.mapper.movie

import io.fajarca.project.moviemate.abstraction.Mapper
import io.fajarca.project.moviemate.data.response.movies.TopRatedMovieDto
import io.fajarca.project.moviemate.domain.entity.movielist.Movie
import javax.inject.Inject

class TopRatedMovieMapper @Inject constructor() : Mapper<TopRatedMovieDto, List<Movie>>() {

    override fun map(input: TopRatedMovieDto): List<Movie> {
        val movies = mutableListOf<Movie>()
        input.results?.forEach {
            movies.add(
                Movie(
                    it?.id ?: 0,
                    it?.title ?: "",
                    it?.originalTitle ?: "",
                    it?.overview ?: "",
                    it?.posterPath ?: "",
                    it?.backdropPath ?: "",
                    it?.voteCount ?: 0,
                    it?.voteAverage ?: 0.0f,
                    it?.popularity ?: 0.0f,
                    it?.releaseDate ?: "",
                    it?.adult ?: false
                )
            )
        }
        return movies
    }
}
