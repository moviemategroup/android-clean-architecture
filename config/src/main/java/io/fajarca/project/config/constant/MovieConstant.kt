package io.fajarca.project.config.constant

object MovieConstant {
    const val IMAGE_BASE_URL_POSTER = "https://image.tmdb.org/t/p/w185/"
    const val IMAGE_BASE_URL_BACKDROP = "https://image.tmdb.org/t/p/w1280"
    const val GLIDE_THUMBNAIL_SIZE_MULTIPLIER = 0.1f
}
